import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetaOkupasiListComponent } from './peta-okupasi-list.component';

describe('PetaOkupasiListComponent', () => {
  let component: PetaOkupasiListComponent;
  let fixture: ComponentFixture<PetaOkupasiListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetaOkupasiListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetaOkupasiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
