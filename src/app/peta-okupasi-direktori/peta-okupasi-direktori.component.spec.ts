import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetaOkupasiDirektoriComponent } from './peta-okupasi-direktori.component';

describe('PetaOkupasiDirektoriComponent', () => {
  let component: PetaOkupasiDirektoriComponent;
  let fixture: ComponentFixture<PetaOkupasiDirektoriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetaOkupasiDirektoriComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetaOkupasiDirektoriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
