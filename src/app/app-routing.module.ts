import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Error404Component } from './errors/error404/error404.component';
import { PetaOkupasiDirektoriComponent } from './peta-okupasi-direktori/peta-okupasi-direktori.component';
import { PetaOkupasiListComponent } from './peta-okupasi-list/peta-okupasi-list.component';
import { PetaOkupasiLitAreaComponent } from './peta-okupasi-lit-area/peta-okupasi-lit-area.component';
import { SettingMasterDataComponent } from './setting-master-data/setting-master-data.component';
import { SettingUserManagementComponent } from './setting-user-management/setting-user-management.component';
import { SkkniDirektoriComponent } from './skkni-direktori/skkni-direktori.component';
import { SkkniListUnitKompetensiComponent } from './skkni-list-unit-kompetensi/skkni-list-unit-kompetensi.component';

const routes: Routes = [
  { path: '', redirectTo:'dashboard',pathMatch:'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'skkni-direktori', component: SkkniDirektoriComponent },
  { path: 'skkni-list-unit-kompetensi', component: SkkniListUnitKompetensiComponent },
  { path: 'peta-okupasi-direktori', component: PetaOkupasiDirektoriComponent },
  { path: 'peta-okupasi-list-area', component: PetaOkupasiLitAreaComponent },
  { path: 'peta-okupasi-list', component: PetaOkupasiListComponent },
  { path: 'setting-master-data', component: SettingMasterDataComponent },
  { path: 'setting-user-management', component: SettingUserManagementComponent },
  { path: '**', component: Error404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
