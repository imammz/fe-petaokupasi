import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetaOkupasiLitAreaComponent } from './peta-okupasi-lit-area.component';

describe('PetaOkupasiLitAreaComponent', () => {
  let component: PetaOkupasiLitAreaComponent;
  let fixture: ComponentFixture<PetaOkupasiLitAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PetaOkupasiLitAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PetaOkupasiLitAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
