import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScrollTipComponent } from './scroll-tip.component';

describe('ScrollTipComponent', () => {
  let component: ScrollTipComponent;
  let fixture: ComponentFixture<ScrollTipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScrollTipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollTipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
