import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkkniDirektoriComponent } from './skkni-direktori.component';

describe('SkkniDirektoriComponent', () => {
  let component: SkkniDirektoriComponent;
  let fixture: ComponentFixture<SkkniDirektoriComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkkniDirektoriComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkkniDirektoriComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
