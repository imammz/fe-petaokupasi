import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkkniListUnitKompetensiComponent } from './skkni-list-unit-kompetensi.component';

describe('SkkniListUnitKompetensiComponent', () => {
  let component: SkkniListUnitKompetensiComponent;
  let fixture: ComponentFixture<SkkniListUnitKompetensiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkkniListUnitKompetensiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkkniListUnitKompetensiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
