import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Error404Component } from './errors/error404/error404.component';
import { MenuComponent } from './nav/menu/menu.component';
import { SkkniDirektoriComponent } from './skkni-direktori/skkni-direktori.component';
import { SkkniListUnitKompetensiComponent } from './skkni-list-unit-kompetensi/skkni-list-unit-kompetensi.component';
import { PetaOkupasiDirektoriComponent } from './peta-okupasi-direktori/peta-okupasi-direktori.component';
import { PetaOkupasiLitAreaComponent } from './peta-okupasi-lit-area/peta-okupasi-lit-area.component';
import { PetaOkupasiListComponent } from './peta-okupasi-list/peta-okupasi-list.component';
import { SettingMasterDataComponent } from './setting-master-data/setting-master-data.component';
import { SettingUserManagementComponent } from './setting-user-management/setting-user-management.component';
import { HeaderComponent } from './nav/header/header.component';
import { FooterComponent } from './nav/footer/footer.component';
import { ScrollTipComponent } from './nav/scroll-tip/scroll-tip.component';
import { HeaderToolbarComponent } from './nav/header-toolbar/header-toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    Error404Component,
    MenuComponent,
    SkkniDirektoriComponent,
    SkkniListUnitKompetensiComponent,
    PetaOkupasiDirektoriComponent,
    PetaOkupasiLitAreaComponent,
    PetaOkupasiListComponent,
    SettingMasterDataComponent,
    SettingUserManagementComponent,
    HeaderComponent,
    FooterComponent,
    ScrollTipComponent,
    HeaderToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
