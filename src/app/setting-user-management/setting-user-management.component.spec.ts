import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingUserManagementComponent } from './setting-user-management.component';

describe('SettingUserManagementComponent', () => {
  let component: SettingUserManagementComponent;
  let fixture: ComponentFixture<SettingUserManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingUserManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingUserManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
